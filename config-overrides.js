const { addWebpackAlias, override } = require('customize-cra');

const path = require('path');

module.exports = override(
  addWebpackAlias({
    Components: path.resolve(__dirname, './src/Components'),
    Assets: path.resolve(__dirname, './src/Assets'),
    MockData: path.resolve(__dirname, './src/MockData'),
    Common: path.resolve(__dirname, './src/Common'),
    Constants: path.resolve(__dirname, './src/Constants'),
    Core: path.resolve(__dirname, './src/Core'),
    CoreFront: path.resolve(__dirname, './src/CoreFront'),
    Pages: path.resolve(__dirname, './src/Pages'),
  })
);
