module.exports = {
  parser: 'babel-eslint',
  extends: ['airbnb', 'prettier', 'plugin:react-hooks/recommended'],
  plugins: ['prettier', 'react-hooks'],
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
      modules: true,
      failOnError: true,
      emitError: false,
      eject: false,
    },
  },
  settings: {
    'import/resolver': {
      alias: {
        map: [
          ['Assets', './src/Assets'],
          ['MockData', './src/MockData'],
          ['Components', './src/Components'],
          ['Common', './src/Common'],
          ['Constants', './src/Constants'],
          ['Core', './src/Core'],
          ['CoreFront', './src/CoreFront'],
          ['Pages', './src/Pages'],
        ],
        extensions: ['.ts', '.js', '.jsx', '.json'],
      },
    },
  },

  rules: {
    'prettier/prettier': ['error'],
    'react/react-in-jsx-scope': 'off',
    'react/jsx-filename-extension': [1, { extensions: ['.js', '.jsx'] }],
    'linebreak-style': 0,
    'no-underscore-dangle': 0,
    'arrow-body-style': 0,
    'jsx-a11y/anchor-is-valid': 0,
    'jsx-a11y/click-events-have-key-events': 0,
    'jsx-a11y/no-noninteractive-element-interactions': 0,
    'jsx-a11y/no-static-element-interactions': 0,
    'prefer-promise-reject-errors': ['error', { allowEmptyReject: true }],
    'react/destructuring-assignment': 0,
    'react/jsx-props-no-spreading': 0,
    camelcase: 0,
  },
};
