fix err : eslint error  at screen
https://github.com/facebook/create-react-app/issues/9887



Config eslint:  npx eslint --init
npm install husky --save-dev
git remote add origin git@gitlab.com:HoangKhanh203/react_resort.git

  "lint-staged": {
    "**/*.{css,scss}": [
      "stylelint --fix",
      "prettier --write",
      "git add"
    ],
    "**/*.{js,jsx}": [
      "eslint --fix",
      "git add"
    ]
  }

For componentDidMount
chạy 1 lần trước khi render html
useEffect(() => {
  // Your code here
}, []);

For componentDidUpdate
// chạy khi biến yourDependency thay đổi
useEffect(() => {
  // Your code here
}, [yourDependency]);
