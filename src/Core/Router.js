import React from 'react';
import { Route, Switch } from 'react-router-dom';
import { urlHelper } from 'Common/utils';
import HomePage from 'Pages/HomePage';
import Account from 'Pages/AccountPage';
import Detail from 'Pages/DetailPage';
import ListOrderPage from 'Pages/ListOrderPage';
import StatusRoomOrderedPage from 'Pages/MyBookingPage';

const AppRouter = () => (
  <Switch>
    <Route path="/" exact component={HomePage} />
    <Route path="/hotels" exact component={HomePage} />
    <Route path="/account" exact component={Account} />
    <Route
      path={urlHelper.getUrlToDetails().route.path}
      exact
      component={Detail}
    />
    <Route
      path={urlHelper.getUrlToListOrder().route.path}
      exact
      component={ListOrderPage}
    />
    <Route
      path={urlHelper.getUrlToMyBooking().route.path}
      exact
      component={StatusRoomOrderedPage}
    />

    {/* <Route path="/query" exact component={components} />
    <Route path="/query/query" component={components} />
    <Route path="/query/:params" component={components} /> */}
  </Switch>
);
export default AppRouter;
