import API_USERS from './users.api';
import API_ROOMS from './rooms.api';

export default {
  API_USERS,
  API_ROOMS,
};
