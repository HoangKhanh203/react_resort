/* eslint-disable import/prefer-default-export */
import _lodash from 'lodash';
import { DOMAIN, METHOD_API, handleAPI } from './common.api';

const GET_LIST_ROOMS = () =>
  handleAPI({
    url: `${DOMAIN}/get-list-rooms`,
    method: METHOD_API.GET,
  });

const GET_DETAIL_ROOMS = (_obj) => {
  const id_manager_room = _lodash.get(_obj, 'data.id_manager_room', '');
  return handleAPI({
    url: `${DOMAIN}/get-detail-room/${id_manager_room}`,
    method: METHOD_API.GET,
  });
};

const ORDER_BOOKING = (_obj) =>
  handleAPI({
    url: `${DOMAIN}/order-booking`,
    method: METHOD_API.POST,
    data: _obj.data || {},
  });

const GET_LIST_ORDER_BOOKING = () =>
  handleAPI({
    url: `${DOMAIN}/get-list-order-booking`,
    method: METHOD_API.GET,
  });

const GET_ORDER_BOOKING = (_obj) =>
  handleAPI({
    url: `${DOMAIN}/get-order-booking/${_obj?.data?.id_detail_booking}`,
    method: METHOD_API.GET,
  });

const DEPOSIT_ROOMS = (_obj) =>
  handleAPI({
    url: `${DOMAIN}/deposit-rooms`,
    method: METHOD_API.POST,
    data: _obj.data || {},
  });

const GET_MY_BOOKING = () =>
  handleAPI({
    url: `${DOMAIN}/get-my-booking`,
    method: METHOD_API.GET,
  });

const REMOVE_ORDER_BOOKING_BY_ID = (_obj) =>
  handleAPI({
    url: `${DOMAIN}/remove-order-booking-by-id`,
    method: METHOD_API.POST,
    data: _obj.data || {},
  });

const CANCEL_BOOKING_BY_ID = (_obj) =>
  handleAPI({
    url: `${DOMAIN}/cancel-booking-by-id`,
    method: METHOD_API.POST,
    data: _obj.data || {},
  });

const GET_LIST_SERVICE = () =>
  handleAPI({
    url: `${DOMAIN}/get-list-services`,
    method: METHOD_API.GET,
  });

export default {
  GET_LIST_ROOMS,
  GET_DETAIL_ROOMS,
  ORDER_BOOKING,
  GET_LIST_ORDER_BOOKING,
  DEPOSIT_ROOMS,
  GET_ORDER_BOOKING,
  GET_MY_BOOKING,
  REMOVE_ORDER_BOOKING_BY_ID,
  CANCEL_BOOKING_BY_ID,
  GET_LIST_SERVICE,
};
