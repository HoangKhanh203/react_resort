/* eslint-disable import/prefer-default-export */
import { DOMAIN, METHOD_API, handleAPI } from './common.api';

const SIGN_IN = (_data) =>
  handleAPI({
    url: `${DOMAIN}/signin`,
    method: METHOD_API.POST,
    data: _data,
  });

const SIGN_UP = (_data) =>
  handleAPI({
    url: `${DOMAIN}/signup`,
    method: METHOD_API.POST,
    data: _data,
  });

const GET_INFO_USER = () =>
  handleAPI({
    url: `${DOMAIN}/get-info-user`,
    method: METHOD_API.GET,
  });

export default { SIGN_IN, SIGN_UP, GET_INFO_USER };
