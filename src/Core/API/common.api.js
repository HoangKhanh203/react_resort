/* eslint-disable no-undef */
/* eslint-disable import/prefer-default-export */
import Axios from 'axios';
import _lodash from 'lodash';

const DOMAIN = 'http://localhost:5000';
const METHOD_API = {
  POST: 'POST',
  GET: 'GET',
};
const handleAPI = (_obj) => {
  const _urlAPI = _lodash.get(_obj, 'url', DOMAIN);
  const _data = _lodash.get(_obj, 'data', {});
  const _method = _lodash.get(_obj, 'method', METHOD_API.POST);

  return Axios({
    method: _method,
    url: _urlAPI,
    data: _data,
    headers: {
      authorization: localStorage.getItem('TOKEN'),
    },
  })
    .then((res) => res)
    .catch((err) => err);
};

export { DOMAIN, handleAPI, METHOD_API };
