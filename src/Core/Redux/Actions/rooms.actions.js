import _lodash from 'lodash';
import CallAPI from 'Core/API';
import { ROOMS_TYPE } from 'Core/Redux/ConstType';
import { COMMON } from 'Constants';

const getListRooms = () => (dispatch) => {
  dispatch({
    type: ROOMS_TYPE.GET_LIST_ROOMS_REQUEST,
  });
  CallAPI.API_ROOMS.GET_LIST_ROOMS().then((res) => {
    const dataRes = _lodash.get(res, 'data', {});
    if (dataRes.status === COMMON.STATUS_API.SUCCESS) {
      dispatch({
        type: ROOMS_TYPE.GET_LIST_ROOMS_SUCCESS,
        payload: {
          listRooms: _lodash.get(dataRes, 'data', []),
        },
      });
    }
  });
};

const getListService = () => (dispatch) => {
  CallAPI.API_ROOMS.GET_LIST_SERVICE().then((res) => {
    const dataRes = _lodash.get(res, 'data', {});
    if (dataRes.status === COMMON.STATUS_API.SUCCESS) {
      dispatch({
        type: ROOMS_TYPE.GET_LIST_SERVICE_SUCCESS,
        payload: {
          listService: _lodash.get(dataRes, 'data', []),
        },
      });
    }
  });
};

const getDetailRooms = (_obj) => (dispatch) => {
  dispatch({
    type: ROOMS_TYPE.GET_DETAIL_ROOMS_REQUEST,
  });
  return CallAPI.API_ROOMS.GET_DETAIL_ROOMS(_obj).then((res) => {
    const dataRes = _lodash.get(res, 'data', {});
    if (dataRes.status === COMMON.STATUS_API.SUCCESS) {
      dispatch({
        type: ROOMS_TYPE.GET_DETAIL_ROOMS_SUCCESS,
        payload: {
          detailRooms: _lodash.get(dataRes, 'data', []),
        },
      });
    }
  });
};

const actOrderBookingRoom = (_data) => {
  return CallAPI.API_ROOMS.ORDER_BOOKING({
    data: _data,
  })
    .then((res) => res)
    .catch((err) => err);
};

const getListOrderBooking = () => (dispatch) => {
  return CallAPI.API_ROOMS.GET_LIST_ORDER_BOOKING().then((res) => {
    const { data } = res;
    return dispatch({
      type: ROOMS_TYPE.GET_LIST_ORDER_BOOKING_SUCCESS,
      payload: {
        dataOrderBooking: data.data,
      },
    });
  });
};

const getOrderBooking = (_obj) => (dispatch) => {
  return CallAPI.API_ROOMS.GET_ORDER_BOOKING({
    data: {
      id_detail_booking: _obj.id_detail_booking,
    },
  }).then((res) => {
    const { data } = res;
    return dispatch({
      type: ROOMS_TYPE.GET_ORDER_BOOKING_SUCCESS,
      payload: {
        dataOrderBooking: data.data,
      },
    });
  });
};
const depositRooms = (_obj) =>
  CallAPI.API_ROOMS.DEPOSIT_ROOMS({ ..._obj }).then((res) => {
    const { data } = res;
    return data;
  });

const getMyBooking = () => (dispatch) => {
  return CallAPI.API_ROOMS.GET_MY_BOOKING().then((res) => {
    const { data } = res;
    return dispatch({
      type: ROOMS_TYPE.GET_MY_BOOKING_SUCCESS,
      payload: {
        dataMyBooking: data.data,
      },
    });
  });
};

const removeOrderBookingByID = (_obj) => {
  return CallAPI.API_ROOMS.REMOVE_ORDER_BOOKING_BY_ID({
    data: {
      id_detail_booking: _obj.id_detail_booking || '',
    },
  }).then((res) => res.data);
};

const cancelBooking = (_obj) => {
  console.log(_obj);
  return CallAPI.API_ROOMS.CANCEL_BOOKING_BY_ID({
    data: {
      id_detail_booking: _obj.id_detail_booking || '',
    },
  }).then((res) => res.data);
};

export default {
  getListRooms,
  getDetailRooms,
  actOrderBookingRoom,
  getListOrderBooking,
  depositRooms,
  getOrderBooking,
  getMyBooking,
  removeOrderBookingByID,
  cancelBooking,
  getListService,
};
