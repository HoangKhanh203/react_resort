import Users from './users.actions';
import Rooms from './rooms.actions';

export default {
  Users,
  Rooms,
};
