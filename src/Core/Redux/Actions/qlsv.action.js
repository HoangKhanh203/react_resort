/* eslint-disable import/prefer-default-export */
import { QLSV_TYPE } from '../ConstType';

export const actAddSV = (sv) => (dispatch) => {
  const dataPayload = {
    type: QLSV_TYPE.ADD_SV_SUCCESS,
    payload: {
      newSV: sv,
    },
  };
  dispatch(dataPayload);
};

export const actUpdateSV = (_dataSV) => (dispatch) => {
  const dataPayload = {
    type: QLSV_TYPE.UPDATE_SV_SUCCESS,
    payload: {
      dataSV: _dataSV,
    },
  };
  dispatch(dataPayload);
};
