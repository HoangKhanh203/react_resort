/* eslint-disable import/prefer-default-export */
import CallAPI from 'Core/API';

import { LOCAL_STOGARE } from 'Constants';
import { USERS_TYPE } from 'Core/Redux/ConstType';

const actSignIn = (_obj) => (dispatch) => {
  const requestDispatch = {
    type: USERS_TYPE.SIGNIN_REQUEST,
    payload: '',
  };
  dispatch(requestDispatch);
  CallAPI.API_USERS.SIGN_IN(_obj)
    .then((res) => {
      const { token } = res.data;
      console.log(res.data);
      // eslint-disable-next-line no-undef
      localStorage.setItem(LOCAL_STOGARE.TOKEN, token);
      const successDispatch = {
        type: USERS_TYPE.SIGNIN_SUCCESS,
        payload: {
          token,
          status: res.data.status,
        },
      };
      dispatch(successDispatch);
    })
    .catch((err) => {
      const errorDispatch = {
        type: USERS_TYPE.SIGNIN_REQUEST,
        payload: {
          error: err,
        },
      };
      dispatch(errorDispatch);
    });
};

const actSignUp = (_obj) => (dispatch) => {
  const requestDispatch = {
    type: USERS_TYPE.SIGNUP_REQUEST,
    payload: '',
  };
  dispatch(requestDispatch);

  CallAPI.API_USERS.SIGN_UP(_obj)
    .then((res) => {
      const token = res.data.auth_token;

      // eslint-disable-next-line no-undef
      localStorage.setItem(LOCAL_STOGARE.TOKEN, token);
      const successDispatch = {
        type: USERS_TYPE.SIGNUP_SUCCESS,
        payload: {
          token,
          status: res.data.status,
        },
      };
      dispatch(successDispatch);
    })
    .catch((err) => {
      const errorDispatch = {
        type: USERS_TYPE.SIGNUP_FAILURE,
        payload: {
          error: err,
        },
      };
      dispatch(errorDispatch);
    });
};

const cleanReducers = () => (dispatch) => {
  dispatch({
    type: USERS_TYPE.CLEAN_SUCCESS,
  });
};

const actGetInfoUser = () => (dispatch) => {
  CallAPI.API_USERS.GET_INFO_USER()
    .then((res) => {
      // eslint-disable-next-line no-undef
      const successDispatch = {
        type: USERS_TYPE.GET_INFO_USER_SUCCESS,
        payload: {
          dataUser: res.data.data,
        },
      };
      dispatch(successDispatch);
    })
    .catch((err) => {
      const errorDispatch = {
        type: USERS_TYPE.GET_INFO_USER_REQUEST,
        payload: {
          error: err,
        },
      };
      dispatch(errorDispatch);
    });
};

export default {
  actSignIn,
  actSignUp,
  cleanReducers,
  actGetInfoUser,
};
