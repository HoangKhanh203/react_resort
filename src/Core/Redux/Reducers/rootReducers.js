import { combineReducers } from 'redux';
import test from './test';
import qlsv from './qlsv.reducer';
import users from './users.reducer';
import rooms from './rooms.reducer';

const rootReducer = combineReducers({
  test,
  qlsv,
  users,
  rooms,
});

export default rootReducer;
