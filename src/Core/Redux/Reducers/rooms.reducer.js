import _lodash from 'lodash';

import { ROOMS_TYPE } from '../ConstType';

const initialState = {
  loading: false,
  message: '',
  error: false,
  listRooms: [],
  detailRooms: {},
  dataOrderBooking: [],
  dataMyBooking: [],
  listService: [],
};

const RoomsReducers = (state = initialState, action) => {
  switch (action.type) {
    case ROOMS_TYPE.GET_LIST_ROOMS_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case ROOMS_TYPE.GET_LIST_ROOMS_SUCCESS:
      return {
        ...state,
        loading: false,
        listRooms: _lodash.get(action, 'payload.listRooms'),
      };
    case ROOMS_TYPE.GET_LIST_ROOMS_FAILURE:
      return {
        ...state,
        loading: false,
        error: action?.payload?.error || 'ERR_404',
      };

    case ROOMS_TYPE.GET_LIST_SERVICE_SUCCESS:
      return {
        ...state,
        loading: false,
        listService: _lodash.get(action, 'payload.listService'),
      };

    // GET_DETAIL_ROOMS
    case ROOMS_TYPE.GET_DETAIL_ROOMS_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case ROOMS_TYPE.GET_DETAIL_ROOMS_SUCCESS:
      return {
        ...state,
        loading: false,
        detailRooms: _lodash.get(action, 'payload.detailRooms'),
      };
    case ROOMS_TYPE.GET_DETAIL_ROOMS_FAILURE:
      return {
        ...state,
        loading: false,
        error: action?.payload?.error || 'ERR_404',
      };

    case ROOMS_TYPE.GET_LIST_ORDER_BOOKING_SUCCESS:
      return {
        ...state,
        loading: false,
        dataOrderBooking: _lodash.get(action, 'payload.dataOrderBooking', []),
      };
    case ROOMS_TYPE.GET_ORDER_BOOKING_SUCCESS:
      return {
        ...state,
        loading: false,
        dataOrderBooking: _lodash.get(action, 'payload.dataOrderBooking', {}),
      };
    case ROOMS_TYPE.GET_MY_BOOKING_SUCCESS:
      return {
        ...state,
        loading: false,
        dataMyBooking: _lodash.get(action, 'payload.dataMyBooking', []),
      };
    default:
      return state;
  }
};

export default RoomsReducers;
