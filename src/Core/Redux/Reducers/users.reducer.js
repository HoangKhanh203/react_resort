import { USERS_TYPE } from '../ConstType';

const initialState = {
  loading: false,
  message: '',
  error: false,
  token: '',
  authenticated: false,
  statusAuthen: '',
  dataUser: {},
};

const AuthReducers = (state = initialState, action) => {
  switch (action.type) {
    case USERS_TYPE.SIGNIN_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case USERS_TYPE.SIGNIN_SUCCESS:
      return {
        ...state,
        loading: false,
        token: action?.payload?.token || '',
        authenticated: true,
        statusAuthen: action?.payload?.status || '',
        dataUser: action?.payload || {},
      };
    case USERS_TYPE.SIGNIN_FAILURE:
      return {
        ...state,
        loading: false,
        error: action?.payload?.error || 'ERR_404',
      };

    case USERS_TYPE.SIGNUP_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case USERS_TYPE.SIGNUP_SUCCESS:
      return {
        ...state,
        loading: false,
        token: action?.payload?.token || '',
        statusAuthen: action?.payload?.status || '',
        authenticated: true,
      };
    case USERS_TYPE.SIGNUP_FAILURE:
      return {
        ...state,
        loading: false,
        error: action?.payload?.error || 'ERR_404',
      };
    case USERS_TYPE.GET_INFO_USER_SUCCESS:
      return {
        ...state,
        dataUser: action?.payload?.dataUser || {},
      };
    case USERS_TYPE.CLEAN_SUCCESS:
      return {
        ...state,
        loading: false,
        error: null,
        authenticated: null,
        statusAuthen: '',
        token: '',
      };

    default:
      return state;
  }
};

export default AuthReducers;
