import { QLSV_TYPE } from '../ConstType';

const initialState = {
  loading: false,
  message: '',
  error: false,
  dataSV: [],
};

const QLSVReducers = (state = initialState, action) => {
  switch (action.type) {
    case QLSV_TYPE.ADD_SV_SUCCESS:
      return {
        ...state,
        dataSV: [...state.dataSV, action.payload.newSV],
      };

    case QLSV_TYPE.UPDATE_SV_SUCCESS:
      return {
        ...state,
        dataSV: [...action.payload.dataSV],
      };
    default:
      return state;
  }
};

export default QLSVReducers;
