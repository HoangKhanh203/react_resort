import { createConstType } from 'Common/helper';

const ROOMS_TYPE = {
  ...createConstType('ROOMS', 'GET_LIST_ROOMS'),
  ...createConstType('ROOMS', 'GET_DETAIL_ROOMS'),
  ...createConstType('ROOMS', 'GET_LIST_ORDER_BOOKING'),
  ...createConstType('ROOMS', 'GET_ORDER_BOOKING'),
  ...createConstType('ROOMS', 'GET_MY_BOOKING'),
  ...createConstType('ROOMS', 'GET_LIST_SERVICE'),
};
export default ROOMS_TYPE;
