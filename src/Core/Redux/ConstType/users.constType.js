import { createConstType } from 'Common/helper';

const USERS_TYPE = {
  ...createConstType('AUTHEN', 'SIGNIN'),
  ...createConstType('AUTHEN', 'SIGNUP'),
  ...createConstType('AUTHEN', 'CLEAN'),
  ...createConstType('AUTHEN', 'GET_INFO_USER'),
};
export default USERS_TYPE;
