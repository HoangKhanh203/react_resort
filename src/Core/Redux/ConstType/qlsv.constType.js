import { createConstType } from 'Common/helper';

const QLSV_TYPE = {
  ...createConstType('QLSV', 'ADD_SV'),
  ...createConstType('QLSV', 'UPDATE_SV'),
};
export default QLSV_TYPE;
