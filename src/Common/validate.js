/* eslint-disable import/prefer-default-export */
export const isRequired = (required = true, message = 'Vui lòng nhập') => ({
  required,
  message,
});
export const typeInput = (type, message) => ({
  type,
  message,
});
export const RGXFormat = (regx, email, message) => ({
  pattern: regx,
  email,
  message,
});
export const isMatchValue = (valuePre, message) => ({
  validator: async (rule, valueCurrent) => {
    if (valuePre === valueCurrent) return Promise.resolve();
    return Promise.reject(message);
  },
});
