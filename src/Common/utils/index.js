/* eslint-disable import/prefer-default-export */
import { getUrl } from 'Common/helper';

export const urlHelper = {
  getUrlHomePage: () => ({
    route: {
      to: '/',
    },
  }),
  getUrlAccountPage: () => ({
    route: {
      to: '/account',
      path: '/account',
    },
  }),
  getUrlToHotels: () => ({
    route: {
      to: '/hotels',
    },
  }),
  getUrlToRestaurants: () => ({
    route: {
      to: '/restaurants',
    },
  }),
  getUrlToVillas: () => ({
    route: {
      to: '/villas',
    },
  }),
  getUrlToTravels: () => ({
    route: {
      to: '/travels',
    },
  }),
  getUrlToDetails: (_id) => ({
    route: {
      to: `/detail/${_id}`,
      path: '/detail/:id_manager_room',
    },
  }),
  getUrlToListOrder: () => ({
    route: {
      to: `/listorder`,
      path: '/listorder',
      // /:id_room
    },
  }),
  getUrlToMyBooking: () => ({
    route: {
      to: `/my-booking`,
      path: '/my-booking',
    },
  }),
  getUrlToDespositBooking: (_obj) => ({
    route: {
      to: `/deposit/${_obj?.id_detail_booking}`,
      path: '/deposit/:id_detail_booking',
      // /:id_room
    },
  }),
  getUrlHaveParams: (type, number) => {
    const createParams = {
      type,
      number,
    };
    const paramStrUrl = getUrl(createParams);
    return {
      route: {
        to: `/khanh?${paramStrUrl}`,
      },
    };
  },
};
