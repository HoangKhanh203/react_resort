import Account from 'Components/Account';
import React from 'react';

const AccountPage = (props) => {
  return <Account {...props} />;
};
export default AccountPage;
