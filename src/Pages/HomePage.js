import React from 'react';

import Landing from 'Components/Landing';

const HomePage = (props) => {
  return <Landing {...props} />;
};
export default HomePage;
