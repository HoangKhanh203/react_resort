import DetailRooms from 'Components/DetailRooms/index';

const DetailPage = (props) => {
  return <DetailRooms {...props} />;
};

export default DetailPage;
