/* eslint-disable import/prefer-default-export */
const TYPE_ROOM = {
  MODEL: {
    HOTEL: 'HOTEL',
    VILLA: 'VILLA',
  },
};
export { TYPE_ROOM };
