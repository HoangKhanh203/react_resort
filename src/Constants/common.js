/* eslint-disable import/prefer-default-export */
const TYPE_FETCH_API = {
  POST: 'POST',
  GET: 'GET',
};

const STATUS_API = {
  SUCCESS: 'SUCCESS',
  ERROR: 'ERROR',
};

export { TYPE_FETCH_API, STATUS_API };
