/* eslint-disable react/prop-types */
import React from 'react';
import { Button } from 'antd';
import * as helpers from 'Common/helper';
import moment from 'moment';
import _lodash from 'lodash';
import './cardsignle.scss';

const CardSignle = (props) => {
  const { dataCard } = props;
  return (
    <div className="room_signle">
      <div className="wrap_img">
        <img
          src={`http://localhost:5003/${_lodash.get(
            dataCard,
            'infoBooking.manager_room.info_room.images[0]',
            ''
          )}`}
          alt="example"
        />
      </div>
      <div className="wrap_content">
        <div className="wrap_content__title">
          {_lodash.get(dataCard, '[0]infoBooking.manager_room.info_room.title')}
        </div>
        <div className="wrap_content__price">
          <div className="value">
            {helpers.formatCurrency(
              parseInt(
                _lodash.get(
                  dataCard,
                  'infoBooking.manager_room.info_room.price',
                  ''
                ),
                10
              )
            )}{' '}
            / Đêm
          </div>
        </div>
        <div className="wrap_content__detail">
          <div className="row">
            <div className="wrap_info">
              <div className="info_detail">
                <div className="name">Ngày đặt từ</div>
                <div className="value">
                  {moment(
                    _lodash.get(
                      dataCard,
                      'infoBooking.time_booking_checkin',
                      ''
                    )
                  ).format('DD/MM/yyyy')}
                </div>
              </div>
            </div>
            <div className="wrap_info">
              <div className="info_detail">
                <div className="name right">Ngày đặt đến</div>
                <div className="value right">
                  {moment(
                    _lodash.get(
                      dataCard,
                      'infoBooking.time_booking_checkout',
                      ''
                    )
                  ).format('DD/MM/yyyy')}
                </div>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="wrap_info">
              <div className="info_detail">
                <div className="name">Số đêm</div>
                <div className="value">
                  {parseInt(
                    _lodash.get(dataCard, 'infoBooking.number_day_booking', ''),
                    10
                  )}
                </div>
              </div>
            </div>
            <div className="wrap_info">
              <div className="info_detail">
                <div className="name">Số phòng đặt</div>
                <div className="value">
                  {_lodash.get(dataCard, 'infoBooking.amount_room', 1)}
                </div>
              </div>
            </div>
          </div>

          <Button className="btn--cancel" type="primary">
            Huỷ
          </Button>
        </div>
      </div>
    </div>
  );
};

export default CardSignle;
