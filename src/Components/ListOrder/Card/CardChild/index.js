/* eslint-disable react/prop-types */
import React from 'react';
import { Button, Checkbox } from 'antd';
import * as helpers from 'Common/helper';
import moment from 'moment';

const CardSingle = (props) => {
  const {
    handleChooseBooking,
    handleremoveOrderBooking,

    isChooseBooking,
    id_card,
    image,
    title,
    dateFrom,
    dateTo,
    price,
  } = props;
  return (
    <div className="card_room_list">
      <div className="wrap_img">
        <img src={`http://localhost:5003/${image}`} alt="example" />
      </div>
      <div className="wrap_content">
        <div className="wrap_content__title">{title}</div>
        <div className="wrap_content__detail">
          <div className="row">
            <div className="wrap_date">
              <div className="wrap_info">
                <div className="name">Ngày đặt từ</div>
                <div className="value">
                  {moment(dateFrom).format('DD/MM/yyyy')}
                </div>
              </div>
              <div className="wrap_info">
                <div className="name right">Ngày đặt đến</div>
                <div className="value right">
                  {moment(dateTo).format('DD/MM/yyyy')}
                </div>
              </div>
              <div className="wrap_info">
                <div className="value">
                  {helpers.formatCurrency(parseInt(price, 10))} / Đêm
                </div>
                <div className="name right">
                  <Checkbox
                    checked={isChooseBooking}
                    onChange={() =>
                      handleChooseBooking({
                        id_card,
                      })
                    }
                  />
                </div>
              </div>
            </div>
          </div>

          <div className="row jtf-fe">
            <Button
              className="btn--cancel"
              type="primary"
              onClick={() =>
                handleremoveOrderBooking({
                  id_card,
                })
              }
            >
              Huỷ
            </Button>
          </div>
          {/* <div className="row">
            <div className="name">Số đêm</div>
            <div className="value">
              {parseInt(
                _lodash.get(dataCard, 'infoBooking.number_day_booking', ''),
                10
              )}
            </div>
            <div className="name right">Tổng số tiền</div>
            <div className="value right">
              {helpers.formatCurrency(
                parseInt(
                  _lodash.get(dataCard, 'infoBooking.total_money', ''),
                  10
                )
              )}
            </div>
          </div> */}
        </div>
      </div>
    </div>
  );
};
export default CardSingle;
