import React, { useEffect, useState } from 'react';
import { Button, Form, Input } from 'antd';
import 'antd/dist/antd.css';
import { useDispatch, useSelector } from 'react-redux';
import Actions from 'Core/Redux/Actions';
import _lodash from 'lodash';
import * as helpers from 'Common/helper';
import { useHistory } from 'react-router';
import { urlHelper } from 'Common/utils';
import CardChild from './Card/CardChild';

const ListOrder = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const { dataOrderBooking } = useSelector((state) => state.rooms);
  const [dataBooking, setDataBooking] = useState([]);
  const [totalMoney, setTotalMoney] = useState(0);
  const [totalMoneyDeposit, setTotalMoneyDeposit] = useState(0);

  const handleUpdateTotalMoney = (_arrIdDetailBooking) => {
    let deposit = 0;
    const money = (_arrIdDetailBooking || dataBooking).reduce((pre, item) => {
      let moneyTotal = 0;
      if (item?.isChooseBooking) {
        if (item?.type === 'HOTEL') {
          const amount_room = item?.infoBooking?.amount_room || 0;
          const number_day_booking = item?.infoBooking?.number_day_booking || 0;
          const price = _lodash.get(
            item,
            'infoBooking.manager_room.info_room.price',
            0
          );
          const priceOfRoom = amount_room * number_day_booking * price;

          moneyTotal = pre + priceOfRoom;
          deposit += priceOfRoom * 0.1;
        } else {
          const priceOfService =
            item?.infoBooking?.manager_service?.info_service?.price;
          moneyTotal = pre + priceOfService;
          deposit += priceOfService;
        }
      } else {
        moneyTotal = pre + 0;
      }
      console.log({
        moneyTotal,
        deposit,
      });
      return moneyTotal;
    }, 0);
    setTotalMoneyDeposit(deposit);
    setTotalMoney(money);
  };
  useEffect(() => {
    const addNewValue = dataOrderBooking?.map((item) => ({
      ...item,
      isChooseBooking: false,
    }));

    setDataBooking(addNewValue);
    handleUpdateTotalMoney(addNewValue);
  }, [dataOrderBooking]);

  const handleChooseBooking = (_obj) => {
    const id_detail_booking = _obj?.id_card || '';
    if (!id_detail_booking) return;

    const _dataBooking = [...dataBooking] || [];
    const updateDataBooking = _dataBooking.map((item) => {
      const dataBookingItem = { ...item } || {};
      if (id_detail_booking === dataBookingItem.id_detail_booking) {
        dataBookingItem.isChooseBooking =
          !dataBookingItem.isChooseBooking || false;
      }
      return dataBookingItem;
    });
    handleUpdateTotalMoney(updateDataBooking);
    setDataBooking(updateDataBooking);
  };
  const handleBookingRooms = async (values) => {
    const arrIdDetailBooking = [];
    dataBooking.filter((item) => {
      const { id_detail_booking } = item;

      if (item.isChooseBooking) {
        arrIdDetailBooking.push(id_detail_booking);
      }
      return false;
    });
    const dataDeposit = await Actions.Rooms.depositRooms({
      data: {
        arrIdDetailBooking,
        ...values,
      },
    });
    if (dataDeposit.status === 'SUCCESS') {
      history.push(urlHelper.getUrlToMyBooking().route.to);
    }
  };
  const handleRemoveOrderBooking = async (_obj) => {
    const id_detail_booking = _obj.id_card;
    const data = await Actions.Rooms.removeOrderBookingByID({
      id_detail_booking,
    });
    if (data.status === 'SUCCESS') {
      const _dataBooking = [...dataBooking] || [];
      const posBooking = _dataBooking.findIndex(
        (item) => item.id_detail_booking === id_detail_booking
      );
      _dataBooking.splice(posBooking, 1);
      handleUpdateTotalMoney(_dataBooking);
      setDataBooking(_dataBooking);
    }
  };
  useEffect(() => {
    dispatch(Actions.Rooms.getListOrderBooking());
  }, [dispatch]);
  return (
    <div className="wrap_listorder_component">
      <div className="wrap_listorder_component_container">
        <div className="wrap_layout_booking">
          <div className="header">Danh sách các phòng đã chọn</div>
          <div className="content">
            {dataBooking?.map((item) => {
              const iamgeService = _lodash.get(
                item,
                'infoBooking.manager_service.info_service.images[0]',
                ''
              );
              const titleService = _lodash.get(
                item,
                'infoBooking.manager_service.info_service.title'
              );
              const dateFromService = _lodash.get(
                item,
                'infoBooking.time_booking_checkin',
                Date()
              );
              const dateToService = _lodash.get(
                item,
                'infoBooking.time_booking_checkout',
                Date()
              );
              const priceService = _lodash.get(
                item,
                'infoBooking.manager_service.info_service.price',
                ''
              );

              return (
                <CardChild
                  id_card={_lodash.get(item, 'id_detail_booking', '')}
                  image={_lodash.get(
                    item,
                    'infoBooking.manager_room.info_room.images[0]',
                    iamgeService
                  )}
                  title={_lodash.get(
                    item,
                    'infoBooking.manager_room.info_room.title',
                    titleService
                  )}
                  dateFrom={_lodash.get(
                    item,
                    'infoBooking.time_booking_checkin',
                    dateFromService
                  )}
                  dateTo={_lodash.get(
                    item,
                    'infoBooking.time_booking_checkout',
                    dateToService
                  )}
                  price={_lodash.get(
                    item,
                    'infoBooking.manager_room.info_room.price',
                    priceService
                  )}
                  isChooseBooking={item?.isChooseBooking}
                  handleChooseBooking={handleChooseBooking}
                  handleremoveOrderBooking={handleRemoveOrderBooking}
                />
              );
            })}
          </div>
        </div>
        <Form
          name="normal_login"
          className="form_input_info_user"
          initialValues={{ remember: true }}
          onFinish={handleBookingRooms}
        >
          <div className="wrap_all_item_input">
            <h1>Nhập thông Tin</h1>
            <Form.Item
              name="fullname"
              label="Họ và tên"
              rules={[{ required: true, message: 'Vui lòng nhập họ tên!' }]}
            >
              <Input placeholder="nhập Họ và tên" />
            </Form.Item>
            <Form.Item
              name="phone"
              label="Số điện thoại"
              rules={[
                { required: true, message: 'Vui lòng nhập số điện thoại!' },
              ]}
            >
              <Input placeholder="nhập số điện thoại" />
            </Form.Item>
            <Form.Item
              name="identify"
              label="Chứng minh thư"
              rules={[{ required: true, message: 'Vui lòng nhập CMMND!' }]}
            >
              <Input placeholder="nhập chứng minh thư" />
            </Form.Item>
            {/* <Form.Item
              name="deposit"
              label="Tiền thanh toán"
              rules={[
                { required: true, message: 'Tiền thanh toán ít nhất 10%!' },
              ]}
            >
              <Input placeholder="nhập số Tiền thanh toán" />
            </Form.Item> */}
          </div>
          <div className="detail_price">
            <div className="detail_price_title">
              <h4>Chi tiết giá</h4>
            </div>
            <div className="detail_price_order">
              {dataBooking?.map((item) => {
                if (!item.isChooseBooking) return <> </>;
                const amount_room = item.infoBooking.amount_room || 1;
                const number_day_booking =
                  item.infoBooking.number_day_booking || 1;

                const type = _lodash.get(item, 'type', 'SERVICE', '');

                const title = _lodash.get(
                  item,
                  'infoBooking.manager_room.info_room.title',
                  _lodash.get(
                    item,
                    'infoBooking.manager_service.info_service.title',
                    ''
                  )
                );

                const price = _lodash.get(
                  item,
                  'infoBooking.manager_room.info_room.price',
                  _lodash.get(
                    item,
                    'infoBooking.manager_service.info_service.price',
                    1
                  )
                );

                const priceOfRoom = amount_room * number_day_booking * price;
                return (
                  <div className="item_price">
                    <div className="item_price__container">
                      <div className="item_price__container__title">
                        <div className="txt_title">{title}</div>
                      </div>
                      <div className="amount">
                        {amount_room} {type === 'HOTEL' ? 'phòng' : 'buổi'} x{' '}
                        {number_day_booking} {type === 'HOTEL' ? 'đêm' : 'ngày'}
                      </div>
                      <div className="item_price__container__text_right">
                        {helpers.formatCurrency(priceOfRoom)}
                      </div>
                    </div>
                  </div>
                );
              })}
              <div className="item_price">
                <div className="item_price__container">
                  <div className="item_price__container__title">
                    <div className="txt_title">Tổng giá tiền</div>
                  </div>
                  <div className="amount"> </div>
                  <div className="item_price__container__text_right">
                    {helpers.formatCurrency(parseInt(totalMoney, 10))}
                  </div>
                </div>
              </div>
              <div className="line_horization" />
              <div className="wrap_total_money">
                <p>đặt cọc 10% ( Dịch vụ thanh toán 100% )</p>
                <p className="item_price__container__text_right">
                  {' '}
                  {helpers.formatCurrency(parseInt(totalMoneyDeposit, 10))}
                </p>
              </div>
            </div>
            <div className="wrap_btn_pay">
              <Button
                type="primary"
                className="btn_pay"
                htmlType="submit"
                size="large"
              >
                Thanh Toán
              </Button>
            </div>
          </div>
        </Form>
      </div>
    </div>
  );
};

export default ListOrder;
