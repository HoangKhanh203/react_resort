import React, { useEffect, useRef, useState } from 'react';
import PropTypes from 'prop-types';
import { useDispatch } from 'react-redux';
import { useLocation } from 'react-router-dom';
import { urlHelper } from 'Common/utils';
import io from 'socket.io-client';
import { NotificationHOC } from 'CoreFront';

import Actions from 'Core/Redux/Actions';
import Header from './Header';
import Footer from './Footer';

const LayoutHeaderFooter = (props) => {
  const [isShowHeader, setIsShowHeader] = useState(false);
  const [isShowFooter, setIsShowFooter] = useState(false);

  const refBody = useRef();
  const dispatch = useDispatch();
  const location = useLocation();
  const { pathname = '' } = location;

  useEffect(() => {
    dispatch(Actions.Users.actGetInfoUser());
    const SocketIO = io.connect(
      `http://localhost:5050?token=${JSON.stringify({
        username: 'hoangkhanh2032000@gmail.com',
      })}`,
      {
        withCredentials: true,
        transports: ['websocket'],
        path: '',
      }
    );
    SocketIO.emit('SEND_MESSAGE_CSS', {
      username: 'admin',
    });
    SocketIO.on('NOTI_BOOKING', (data) => {
      props.showNotification({
        type: 'SUCCESS',
        title: 'Thông báo đặt phòng',
        description: data?.mesage || 'Đặt phòng thành công ',
      });
    });
  }, [dispatch]);

  useEffect(() => {
    const arrLinkNotShow = [urlHelper.getUrlAccountPage().route.path];
    const isShow = !arrLinkNotShow.includes(pathname);
    setIsShowHeader(isShow);
    setIsShowFooter(isShow);
  }, [pathname]);
  return (
    <div className="wrap_main_page" ref={refBody}>
      <div className="wrap_main_page_container">
        <header
          className="wrap_main_header"
          style={{ display: isShowHeader ? 'block' : 'none' }}
        >
          <Header />
        </header>
        <main className="wrap_main_body"> {props.children}</main>
        <footer
          className="wrap_main_footer"
          style={{ display: isShowFooter ? 'block' : 'none' }}
        >
          <Footer />
        </footer>
      </div>
    </div>
  );
};
export default NotificationHOC(LayoutHeaderFooter);
LayoutHeaderFooter.propTypes = {
  children: PropTypes.objectOf(PropTypes.any),
  showNotification: PropTypes.func,
};
LayoutHeaderFooter.defaultProps = {
  children: {},
  showNotification: () => {},
};
