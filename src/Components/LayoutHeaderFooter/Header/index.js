/* eslint-disable no-undef */
import React, { useEffect, useMemo, useState } from 'react';
import { Input, AutoComplete, Dropdown } from 'antd';
import { Link, useLocation } from 'react-router-dom';

import { SearchOutlined, DownOutlined } from '@ant-design/icons';
import { get } from 'lodash';
import { urlHelper } from 'Common/utils';
import { useSelector } from 'react-redux';
import UserFeatures from './UserFeatures';

const arrItemNav = [
  {
    title: 'Trang chủ',
    to: urlHelper.getUrlHomePage().route.to,
  },
  {
    title: 'Khách sạn',
    to: urlHelper.getUrlToHotels().route.to,
  },
  {
    title: 'Nhà hàng',
    to: urlHelper.getUrlToRestaurants().route.to,
  },
  {
    title: 'Căn hộ',
    to: urlHelper.getUrlToVillas().route.to,
  },
];
const arrPageShowHeaderAlways = ['/my-booking'];
const Header = () => {
  const location = useLocation();
  const { pathname = '' } = location;
  const { dataUser } = useSelector((state) => state.users);
  const [isScroll, setIsScroll] = useState(false);
  useEffect(() => {
    const checkShowHeader = arrPageShowHeaderAlways.includes(pathname);

    if (checkShowHeader) {
      return setIsScroll(true);
    }
    window.addEventListener('scroll', () => {
      if (window.scrollY > 0) {
        if (!isScroll) setIsScroll(true);
      } else if (!checkShowHeader) {
        setIsScroll(false);
      }
    });
    return () => {};
    // return () => {
    //   window.removeEventListener('scroll');
    // };
  }, [pathname]);

  const renderMenuItemNav = useMemo(
    () =>
      arrItemNav.map((item) => (
        <Link
          to={get(item, 'to', '/')}
          key={item.title}
          className={
            get(item, 'to', '/').includes(pathname) && pathname !== '/'
              ? 'is-active-nav'
              : ''
          }
        >
          <div className="wrap_item_nav">{get(item, 'title', 'Home')}</div>
        </Link>
      )),
    [pathname]
  );
  return (
    <div className="wrap_header">
      <div
        className={`wrap_header_container mode--${
          isScroll ? 'white' : 'transparent'
        }`}
      >
        <div className="wrap_logo_search">
          <Link to="/">
            <div className="wrap_logo">
              <svg width="25" height="27.984" viewBox="0 0 25 27.984">
                <g transform="translate(0 0)">
                  <path
                    d="M25.45,2.767a34.5,34.5,0,0,0-4,1.143,35.262,35.262,0,0,0-3.771,1.545,26.069,26.069,0,0,0-3.179,1.8,26.068,26.068,0,0,0-3.191-1.8A35.262,35.262,0,0,0,7.54,3.909,34.5,34.5,0,0,0,3.55,2.767L2,2.45V17.94a12.5,12.5,0,1,0,25,0V2.45ZM14.5,10.492c2.339,1.96,3.522,4.19,3.512,6.608a3.512,3.512,0,1,1-7.024,0h0C10.98,14.66,12.162,12.442,14.5,10.492Zm9.913,7.448a9.915,9.915,0,0,1-19.831,0V5.69a31.8,31.8,0,0,1,7.748,3.259,13.43,13.43,0,0,0-2.344,2.737A9.929,9.929,0,0,0,8.4,17.095a6.1,6.1,0,1,0,12.2,0,9.932,9.932,0,0,0-1.587-5.412,13.427,13.427,0,0,0-2.346-2.742,29.737,29.737,0,0,1,5.586-2.577c.819-.284,1.559-.51,2.158-.675Z"
                    transform="translate(-2 -2.45)"
                    fill="#fff"
                  />
                </g>
              </svg>
              <div className="title_page">TeamA6</div>
            </div>
          </Link>
          <div className="wrap_input_search">
            <AutoComplete
              style={{ width: '100%' }}
              className="wrap_card_car_take_flight"
              dropdownMatchSelectWidth={500}
              onSelect={(e) => {
                console.log(e);
              }}
              filterOption={(inputValue, option) =>
                get(option, 'value', '')
                  .toUpperCase()
                  .indexOf((inputValue || '').toUpperCase()) !== -1
              }
            >
              <Input />
            </AutoComplete>
            <span className="icon_search">
              <SearchOutlined />
            </span>
          </div>
        </div>
        <div className="wrap_menu_nav">{renderMenuItemNav}</div>
        {dataUser?.fullname ? (
          <div className="isLogin">
            <Dropdown
              overlay={<UserFeatures />}
              trigger={['hover']}
              overlayStyle={{
                zIndex: 99999999,
              }}
            >
              <div className="isLogin__content">
                <div className="wrap_title">{dataUser?.fullname}</div>
                <div className="wrap_icon_drow">
                  <DownOutlined />
                </div>
              </div>
            </Dropdown>
          </div>
        ) : (
          <Link to={urlHelper.getUrlAccountPage().route.to}>
            <div className="wrap_btn">Đăng nhập</div>
          </Link>
        )}
      </div>
    </div>
  );
};
export default Header;
