/* eslint-disable no-undef */
import React from 'react';
import { Button } from 'antd';
import { Link } from 'react-router-dom';
import { urlHelper } from 'Common/utils';
import './userfeatures.scss';

const UserFeatures = () => {
  return (
    <div className="wrap__modal__user">
      <div className="container_modal_user">
        <div className="link-to-page">
          <Link
            to={urlHelper.getUrlToMyBooking().route.to}
            className="link-page"
          >
            Danh sách Booking
          </Link>
        </div>
        <div className="link-to-page">
          <Link
            to={urlHelper.getUrlToListOrder().route.to}
            className="link-page"
          >
            Danh sách Order
          </Link>
        </div>
        <hr className="line-border" />
        <div className="wrap_btn">
          <Button
            type="primary"
            className="btn_logout"
            onClick={() => {
              localStorage.removeItem('TOKEN');
            }}
          >
            Đăng Xuất
          </Button>
        </div>
      </div>
    </div>
  );
};

export default UserFeatures;
