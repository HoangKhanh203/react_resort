import { urlHelper } from 'Common/utils';

const arrItemNav = [
  {
    title: 'Home',
    to: urlHelper.getUrlHomePage().route.to,
  },
  {
    title: 'Hotels',
    to: urlHelper.getUrlToHotels().route.to,
  },
  {
    title: 'Restaurants',
    to: urlHelper.getUrlToRestaurants().route.to,
  },
  {
    title: 'Villas',
    to: urlHelper.getUrlToVillas().route.to,
  },
  {
    title: 'Travels',
    to: urlHelper.getUrlToTravels().route.to,
  },
];
export default arrItemNav;
