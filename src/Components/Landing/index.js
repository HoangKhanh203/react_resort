import React, { useMemo, useEffect } from 'react';
import { Carousel } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import Actions from 'Core/Redux/Actions';
// import MockData from 'MockData';
import _lodash from 'lodash';
// import { urlHelper } from 'Common/utils';
import * as FileConstants from 'Constants';
import CardHover from './CardHover';

// const listMenu = [
//   {
//     title: 'Hotels',
//     image: 'images/img3.jpg',
//     icon: 'la-hotel',
//     to: urlHelper.getUrlToHotels().route.to,
//     amount: 21,
//   },
//   {
//     title: 'Restaurants',
//     icon: 'la-cutlery',
//     to: urlHelper.getUrlToRestaurants().route.to,
//     image: 'images/img1.jpg',

//     amount: 36,
//   },
//   {
//     title: 'Villas',
//     icon: 'la-warehouse',
//     image: 'images/img4.jpg',
//     to: urlHelper.getUrlToVillas().route.to,
//     amount: 16,
//   },
//   {
//     title: 'Travels',
//     image: 'images/img2.jpg',
//     icon: 'la-plane',
//     to: urlHelper.getUrlToTravels().route.to,
//     amount: 3,
//   },
// ];
const arrImageBackgroundCarousel = [
  {
    src: 'https://tripfinder-redq.firebaseapp.com/static/media/1.3bd6d5f8.jpg',
    alt: 'view_1',
    title: 'Chào mừng đến với khu Resort A6',
  },
  {
    src: 'https://tripfinder-redq.firebaseapp.com/static/media/3.b9923e48.jpg',
    alt: 'view_1',
    title: 'Chúng tôi hy vọng bạn sẽ yêu thích Resort A6',
  },
  {
    src: 'https://tripfinder-redq.firebaseapp.com/static/media/2.7ac6e294.jpg',
    alt: 'view_1',
    title: 'Chúc bạn có buổi thư giản tuyệt vời',
  },
];

const Landing = () => {
  const dispatch = useDispatch();
  const { listRooms, listService } = useSelector((state) => state.rooms);

  useEffect(() => {
    dispatch(Actions.Rooms.getListRooms());
    dispatch(Actions.Rooms.getListService());
  }, [dispatch]);
  const renderCardHotels = useMemo(
    () =>
      listRooms.map((item) => {
        const infoRoom = _lodash.get(item, 'info_room', {});

        if (
          _lodash.get(infoRoom, 'type_model', '') ===
          FileConstants.Rooms.TYPE_ROOM.MODEL.HOTEL
        ) {
          return (
            <CardHover
              id={_lodash.get(item, 'id_manager_room', '')}
              rate={_lodash.get(infoRoom, 'rate', '')}
              title={_lodash.get(infoRoom, 'title', '')}
              price={_lodash.get(infoRoom, 'price', 0)}
              images={_lodash.get(infoRoom, 'images', [])}
            />
          );
        }
        return null;
      }),
    [listRooms]
  );

  // const renderCardVilla = useMemo(
  //   () =>
  //     listRooms.map((item) => {
  //       const infoRoom = _lodash.get(item, 'info_room', {});
  //       if (
  //         _lodash.get(infoRoom, 'type_model', '') ===
  //         FileConstants.Rooms.TYPE_ROOM.MODEL.VILLA
  //       ) {
  //         return (
  //           <CardHover
  //             id={_lodash.get(item, 'id_manager_room', '')}
  //             rate={_lodash.get(infoRoom, 'rate', '')}
  //             title={_lodash.get(infoRoom, 'title', '')}
  //             price={_lodash.get(infoRoom, 'price', 0)}
  //             images={_lodash.get(infoRoom, 'images', [])}
  //           />
  //         );
  //       }
  //       return null;
  //     }),
  //   [listRooms]
  // );

  const renderCardService = useMemo(
    () =>
      listService.map((item) => {
        const infoService = _lodash.get(item, 'info_service', {});
        return (
          <CardHover
            id={_lodash.get(item, 'id_manager_service', '')}
            rate={_lodash.get(infoService, 'rate', '')}
            title={_lodash.get(infoService, 'title', '')}
            price={_lodash.get(infoService, 'price', 0)}
            images={_lodash.get(infoService, 'images', [])}
          />
        );
      }),
    [listService]
  );

  // const renderCategories = useMemo(
  //   () =>
  //     listMenu.map((item) => (
  //       <div className="item_cate">
  //         <img
  //           className="img-background"
  //           src={`https://techydevs.com/demos/themes/html/dirto/${item.image}`}
  //           alt="icon"
  //         />
  //         <div className="wrap_info_cate">
  //           <div className="wrap_icon">
  //             <span className={`la ${item.icon}`} />
  //           </div>
  //           <div className="title_name">{item.title}</div>
  //           <div className="amount">{item.amount} Items</div>
  //         </div>
  //       </div>
  //     )),
  //   []
  // );

  return (
    <div className="wrap_landing">
      <div className="wrap_landing_container">
        <div className="wrap_first_screen_page">
          <Carousel autoplay className="wrap_carousel_landing_first">
            {arrImageBackgroundCarousel.map((item) => (
              <div className="wrap_img_background">
                <img src={item.src} alt={item.alt} />
                <div className="title_welcome">{item.title}</div>
              </div>
            ))}
          </Carousel>
        </div>
        <div className="wrap_content">
          {/* <div className="wrap_categories">
              <div className="wrap_cate">{renderCategories}</div>
            </div> */}
          <div className="wrap_type_room">
            <p className="title_type">Phòng khách sạn</p>
            <div className="list_room">{renderCardHotels}</div>
          </div>
          {/* <div className="wrap_type_room">
            <p className="title_type">Căn hộ</p>
            <div className="list_room">{renderCardVilla}</div>
          </div> */}
          {listService.length > 0 && (
            <div className="wrap_type_room">
              <p className="title_type">Dịch Vụ</p>
              <div className="list_room">{renderCardService}</div>
            </div>
          )}
        </div>
      </div>
    </div>
  );
};
export default Landing;
