import React from 'react';
import { Rate, Carousel } from 'antd';

import PropTypes from 'prop-types';
import { urlHelper } from 'Common/utils';
import { formatCurrency } from 'Common/helper';
import { Link } from 'react-router-dom';

const CardHover = (props) => {
  return (
    <div className="card_room_hover">
      <div className="card_room_hover_container">
        <div className="wrap_img">
          <Carousel autoplay>
            {props.images.map((item) => (
              <img src={`http://localhost:5003/${item}`} alt="1" />
            ))}
          </Carousel>
          {/* <div className="wrap_icon_heart" onClick={() => setIsLike(!isLike)}>
            {isLike ? (
              <i className="fas fa-heart" />
            ) : (
              <span className="la la-heart-o" />
            )}
          </div> */}
        </div>
        <div className="wrap_info">
          <div className="title">{props.title}</div>
          <div className="wrap_price">{formatCurrency(props.price)} /đêm</div>
          <div className="wrap_rate">
            <Rate
              style={{ fontSize: '12px', color: 'rgb(1, 127, 255)' }}
              disabled
              defaultValue={props.rate}
            />
          </div>
          <Link to={urlHelper.getUrlToDetails(props.id).route.to}>
            <div className="book_card">Chi tiết</div>
          </Link>
        </div>
      </div>
    </div>
  );
};
export default CardHover;
CardHover.propTypes = {
  id: PropTypes.string,
  title: PropTypes.string,
  price: PropTypes.number,
  rate: PropTypes.number,
  images: PropTypes.arrayOf(PropTypes.any),
};
CardHover.defaultProps = {
  id: '',
  title: 'Villas huhi',
  price: 1000000,
  rate: 5,
  images: [],
};
