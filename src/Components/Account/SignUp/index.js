import React, { useEffect } from 'react';
import { Input, Form, Switch, Button, notification } from 'antd';
import { UserOutlined, LockOutlined } from '@ant-design/icons';
import PropTypes from 'prop-types';
import { useHistory } from 'react-router-dom';
import Actions from 'Core/Redux/Actions';
import * as Validator from 'Common/validate';

import { useDispatch, useSelector } from 'react-redux';
import * as FileConstants from 'Constants';

const SignUp = (props) => {
  const { statusAuthen } = useSelector((state) => state.users);

  const history = useHistory();
  const dispatch = useDispatch();

  useEffect(() => {
    if (statusAuthen === FileConstants.COMMON.STATUS_API.SUCCESS) {
      notification.info({
        message: `TeamA6 Resort`,
        description: 'Đăng kí thành công.',
        placement: 'topRight',
      });
      props.switchLayout();
    } else if (statusAuthen === FileConstants.COMMON.STATUS_API.ERROR) {
      notification.info({
        message: `TeamA6 Resort`,
        description: 'Đăng kí thất bại.',
        placement: 'topRight',
      });
    }
  }, [statusAuthen, history]);

  const onSignUp = (_value) => {
    dispatch(Actions.Users.actSignUp(_value));
  };
  return (
    <div className="wrap_layout_signup">
      <div className="wrap_layout_signup_container">
        <div className="wrap_layout_signup_container_content">
          <div className="title">Resort A6</div>
          <div className="sub_title">Vui lòng đăng kí tài khoản</div>
          <div className="wrap_layout_input">
            <Form onFinish={onSignUp}>
              <Form.Item
                className="wrap_input_account"
                name="email"
                label="Email"
                rules={[Validator.isRequired]}
              >
                <Input placeholder="Nhập Email" prefix={<UserOutlined />} />
              </Form.Item>
              <Form.Item
                className="wrap_input_account"
                name="fullname"
                label="Họ tên"
                rules={[Validator.isRequired]}
              >
                <Input placeholder="Nhập họ tên" prefix={<UserOutlined />} />
              </Form.Item>
              <Form.Item
                className="wrap_input_account"
                name="password"
                label="Mật khẩu"
                rules={[Validator.isRequired]}
              >
                <Input.Password
                  placeholder="Nhập mật khẩu"
                  prefix={<LockOutlined />}
                  rules={[Validator.isRequired]}
                />
              </Form.Item>
              <Form.Item
                className="wrap_input_account"
                name="confirmPassword"
                label="Xác nhận mật khẩu"
                rules={[Validator.isRequired]}
              >
                <Input.Password
                  placeholder="Nhập xác nhận mật khẩu"
                  prefix={<LockOutlined />}
                />
              </Form.Item>
              <div className="wrap_all_switch">
                <Form.Item name="agree_term" className="wrap_switch">
                  <Switch defaultChecked />
                  <span>Tôi đồng ý với chính sách của bạn</span>
                </Form.Item>
              </div>
              <div className="wrap_btn_submit">
                <Button type="primary" htmlType="submit">
                  Đăng kí
                </Button>
              </div>
              <div className="wrap_title_signin">
                đã có tài khoản
                <span
                  className="title_signin"
                  onClick={() => props.switchLayout()}
                >
                  Đăng nhập
                </span>
              </div>
            </Form>
          </div>
        </div>
      </div>
    </div>
  );
};
export default SignUp;
SignUp.propTypes = {
  switchLayout: PropTypes.func,
};
SignUp.defaultProps = {
  switchLayout: () => {},
};
