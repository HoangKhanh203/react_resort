import { urlHelper } from 'Common/utils';
import React, { useCallback, useState } from 'react';
import { Link } from 'react-router-dom';
import BackgroundImage from 'Assets/Images/other/background_account.jpg';
import SignIn from './SignIn';
import SignUp from './SignUp';

const Account = (props) => {
  const [swithToSignUp, setSwitchToSignUp] = useState(false);

  const handleSwitchToSignUp = useCallback(() => setSwitchToSignUp(true), []);
  const handleSwitchToSignIn = useCallback(() => setSwitchToSignUp(false), []);

  return (
    <div className="wrap_account">
      <div className="wrap_account_container">
        <div
          className={`wrap_sign_account ${swithToSignUp && 'switch-signup'}`}
        >
          {swithToSignUp ? (
            <SignUp {...(props || {})} switchLayout={handleSwitchToSignIn} />
          ) : (
            <SignIn {...(props || {})} switchLayout={handleSwitchToSignUp} />
          )}
        </div>
        <div
          className={`wrap_img_bacground ${
            swithToSignUp && 'swith-img-to-left'
          }`}
        >
          <Link to={urlHelper.getUrlHomePage().route.to}>
            <img src={BackgroundImage} alt="background" />
          </Link>
        </div>
      </div>
    </div>
  );
};
export default Account;
