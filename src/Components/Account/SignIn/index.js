import React, { useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { Input, Form, Button } from 'antd';
import { UserOutlined, LockOutlined } from '@ant-design/icons';
import PropTypes from 'prop-types';

import * as Validator from 'Common/validate';
import { urlHelper } from 'Common/utils';

import Actions from 'Core/Redux/Actions';
import * as FileConstants from 'Constants';
import { NotificationHOC } from 'CoreFront';

// const loginWith = [
//   {
//     title: 'Facebook',
//     background: '#3B5998',
//   },
//   {
//     title: 'Google',
//     background: '#DD4B59',
//   },
// ];
const SignIn = (props) => {
  const history = useHistory();
  const dispatch = useDispatch();
  const { showNotification } = props;
  const { statusAuthen } = useSelector((state) => state.users);
  useEffect(() => {
    if (statusAuthen === FileConstants.COMMON.STATUS_API.SUCCESS) {
      // notification.info({
      //   message: `TeamA6 Resort`,
      //   description: 'Đăng nhập thành công.',
      //   placement: 'topRight',
      // });
      showNotification({
        type: 'SUCCESS',
        title: 'Thông báo đăng nhập',
        description: 'Đăng nhập thành công.',
      });
      setTimeout(() => {
        history.push(urlHelper.getUrlHomePage().route.to);
      }, 500);
    } else if (statusAuthen === FileConstants.COMMON.STATUS_API.ERROR) {
      // notification.info({
      //   message: `TeamA6 Resort`,
      //   description: 'Đăng nhập thất bại.',
      //   placement: 'topRight',
      // });
      showNotification({
        type: 'ERROR',
        title: 'Thông báo đăng nhập',
        description: 'Đăng nhập thất bại.',
      });
    }
    dispatch(Actions.Users.cleanReducers());
  }, [statusAuthen, history, showNotification]);

  // const renderBtnLoginWith = useMemo(
  //   () =>
  //     loginWith.map((item) => (
  //       <div
  //         key={item.title}
  //         className="btn_login_with"
  //         style={{ backgroundColor: item.background }}
  //       >
  //         {item.title}
  //       </div>
  //     )),
  //   []
  // );
  const onSignIn = (_values) => {
    dispatch(Actions.Users.actSignIn(_values));
  };
  return (
    <div className="wrap_layout_login">
      <div className="wrap_layout_login_container">
        <div className="wrap_layout_login_container_content">
          <div className="title">Resort A6</div>
          <div className="sub_title">Vui lòng nhập tài khoản</div>
          <div className="wrap_layout_input">
            <Form onFinish={onSignIn}>
              <Form.Item
                className="wrap_input_account"
                name="email"
                label="Email"
                rules={[Validator.isRequired]}
              >
                <Input placeholder="Nhập Email" prefix={<UserOutlined />} />
              </Form.Item>
              <Form.Item
                className="wrap_input_account"
                name="password"
                label="Mật khẩu"
                rules={[Validator.isRequired]}
              >
                <Input.Password
                  placeholder="Nhập mật khẩu"
                  prefix={<LockOutlined />}
                />
              </Form.Item>
              <div
                className="wrap_btn_submit"
                style={{
                  marginBottom: 16,
                }}
              >
                <Button type="primary" htmlType="submit">
                  Đăng nhập
                </Button>
              </div>
              <div className="wrap_title_register">
                Bạn không có tài khoản
                <span
                  className="title_register"
                  onClick={() => props.switchLayout()}
                >
                  Đăng kí
                </span>
              </div>
            </Form>
          </div>
        </div>
      </div>
    </div>
  );
};
export default NotificationHOC(SignIn);
SignIn.propTypes = {
  switchLayout: PropTypes.func,
  showNotification: PropTypes.func,
};
SignIn.defaultProps = {
  switchLayout: () => {},
  showNotification: () => {},
};
