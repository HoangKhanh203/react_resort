import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import _lodash from 'lodash';
import Actions from 'Core/Redux/Actions';
import CardHotel from './CardMyBooking';

const StatusRoomOrder = () => {
  const dispatch = useDispatch();
  const { dataMyBooking } = useSelector((state) => state.rooms);
  const [dataState, setDataState] = useState(dataMyBooking);
  const sortMyBooking = (_data) => {
    const _dataState = JSON.parse(JSON.stringify(_data));
    if (!_dataState) return;

    const addValuePrioritize = _dataState?.map((item) => {
      const dataItem = { ...item } || {};
      switch (dataItem.status) {
        case 'CHECKIN':
          dataItem.prioritize = 2;
          break;
        case 'DEPOSIT':
          dataItem.prioritize = 1;
          dataItem.isCanCancel = true;

          break;
        case 'CHECKOUT':
          dataItem.prioritize = 3;
          dataItem.isCanCancel = false;

          break;
        case 'CANCEL':
          dataItem.prioritize = 4;
          dataItem.isCanCancel = false;

          break;
        default:
          dataItem.prioritize = 5;
          dataItem.isCanCancel = false;
          break;
      }
      return dataItem;
    });
    for (let i = 0; i < addValuePrioritize.length; i += 1) {
      for (let y = i + 1; y < addValuePrioritize.length; y += 1) {
        const tmp = addValuePrioritize[i];
        if (
          addValuePrioritize[i].prioritize > addValuePrioritize[y].prioritize
        ) {
          addValuePrioritize[i] = addValuePrioritize[y];
          addValuePrioritize[y] = tmp;
        }
      }
    }
    setDataState(addValuePrioritize);
  };
  useEffect(() => {
    sortMyBooking(dataMyBooking);
  }, [dataMyBooking]);
  useEffect(() => {
    dispatch(Actions.Rooms.getMyBooking());
  }, []);

  const handleCancelBooking = async (_obj) => {
    const id_detail_booking = _obj?.id_card || '';

    const _dataState = [...dataState] || [];

    // const posBooking = _dataState.findIndex(
    //   (item) => item.id_detail_booking === id_detail_booking
    // );

    // _dataState[posBooking].status = 'CANCEL';
    const infoCancel = await Actions.Rooms.cancelBooking({
      id_detail_booking,
    });
    if (infoCancel.status === 'SUCCESS') {
      sortMyBooking(_dataState);
    }
  };
  return (
    <div className="wrap_page_room_ordered">
      <div className="wrap_page_room_ordered__container">
        <div className="wrap_page_room_ordered__container__content">
          <div className="title_page">Danh sách các phòng đã đặt</div>
          {dataState.map((item) => {
            const imageService = _lodash.get(
              item,
              'infoBooking.manager_service.info_service.images[0]',
              ''
            );
            const titleService = _lodash.get(
              item,
              'infoBooking.manager_service.info_service.title'
            );
            const dateFromService = _lodash.get(
              item,
              'infoBooking.time_booking_checkin',
              Date()
            );
            const dateToService = _lodash.get(
              item,
              'infoBooking.time_booking_checkout',
              Date()
            );
            const status = _lodash.get(item, 'status', 'DEPOSIT');
            const type = _lodash.get(
              item,
              'infoBooking.manager_room.info_room.type_room',
              'Dịch vụ'
            );

            return (
              <CardHotel
                id_card={_lodash.get(item, 'id_detail_booking', '')}
                image={_lodash.get(
                  item,
                  'infoBooking.manager_room.info_room.images[0]',
                  imageService
                )}
                title={_lodash.get(
                  item,
                  'infoBooking.manager_room.info_room.title',
                  titleService
                )}
                guest={_lodash.get(
                  item,
                  'infoBooking.manager_room.info_room.amount_guest',
                  1
                )}
                dateFrom={_lodash.get(
                  item,
                  'infoBooking.time_booking_checkin',
                  dateFromService
                )}
                dateTo={_lodash.get(
                  item,
                  'infoBooking.time_booking_checkout',
                  dateToService
                )}
                amoun_day={_lodash.get(
                  item,
                  'infoBooking.number_day_booking',
                  1
                )}
                type={type}
                status={status}
                isHotel={type === 'HOTEL'}
                total_money={_lodash.get(item, 'payment.total_money', 1)}
                isCanCancel={_lodash.get(item, 'isCanCancel', false)}
                handleCancelBooking={handleCancelBooking}
              />
            );
          })}
        </div>
      </div>
    </div>
  );
};

export default StatusRoomOrder;
