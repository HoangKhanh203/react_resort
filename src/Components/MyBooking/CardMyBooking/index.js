/* eslint-disable react/prop-types */
import React from 'react';
import moment from 'moment';
import * as helpers from 'Common/helper';

const CardHotel = (props) => {
  const {
    id_card,
    title,
    total_money,
    image,
    dateFrom,
    dateTo,
    isHotel,
    status,
    type,
    guest,
    amoun_day,
    isCanCancel,
    handleCancelBooking,
  } = props;

  const checkStatus = (_status) => {
    switch (_status) {
      case 'CHECKIN':
        return `Đã ${isHotel ? 'nhận phòng' : 'sử dụng'}`;
      case 'CHECKOUT':
        return `Đã ${isHotel ? 'trả phòng' : 'thanh toán'}`;
      case 'CANCEL':
        return `Đã ${isHotel ? 'hủy phòng' : 'hủy dịch vụ'}`;
      case 'DEPOSIT':
        return `chưa ${isHotel ? 'nhận phòng' : 'sử dụng dịch vụ'}`;
      default:
        return `chưa ${isHotel ? 'nhận phòng' : 'sử dụng dịch vụ'}`;
    }
  };
  return (
    <div className="wrap_card_booking_room">
      <div className="wrap_img">
        <img src={`http://localhost:5003/${image}`} alt="image_room" />
      </div>
      <div className="wrap_detail">
        <div className="item_detail">
          <div className="value title_main">{title}</div>
        </div>
        <div className="item_detail row_child">
          <div className="title">Số lượng khách: </div>
          <div className="value">{guest}</div>
        </div>
        <div className="item_detail row_child">
          <div className="title">Loại: </div>
          <div className="value">{type}</div>
        </div>
      </div>
      <div className="wrap_detail w-30">
        <div className="row">
          <div className="item_detail">
            <div className="title">
              Sẽ {isHotel ? 'nhận phòng' : 'sử dụng vào'} lúc
            </div>
            <div className="value">{moment(dateFrom).format('DD/MM/yyyy')}</div>
          </div>
          <div className="item_detail right">
            <div className="title">
              Sẽ {isHotel ? 'trả phòng' : 'hết vào'} lúc
            </div>
            <div className="value">{moment(dateTo).format('DD/MM/yyyy')}</div>
          </div>
        </div>
        <div className="row">
          <div className="item_detail">
            <div className="title">Tổng ngày</div>
            <div className="value">{amoun_day}</div>
          </div>
          <div className="item_detail right">
            <div className="title">Tổng số tiền</div>
            <div className="value">{helpers.formatCurrency(total_money)}</div>
          </div>
        </div>
      </div>
      <div className="wrap_detail w-auto">
        <div className="item_detail">
          <div className="title">Tình trạng</div>
          <div
            className="value"
            style={{
              color:
                // eslint-disable-next-line no-nested-ternary
                status === 'CHECKIN'
                  ? 'green'
                  : status === 'DEPOSIT'
                  ? 'blue'
                  : 'red',
            }}
          >
            {checkStatus(status)}
          </div>
        </div>
        {isCanCancel && (
          <div className="item_detail">
            <button
              type="button"
              className="btn_cancel"
              onClick={() =>
                handleCancelBooking({
                  id_card,
                })
              }
            >
              Hủy
            </button>
          </div>
        )}
      </div>
    </div>
  );
};

export default CardHotel;
