import React, { useState, useEffect } from 'react';
import detailBackground from 'Assets/Images/other/detail-background.jpg';
import {
  Rate,
  Input,
  Button,
  Checkbox,
  Form,
  DatePicker,
  Carousel,
  Select,
} from 'antd';
import _lodash from 'lodash';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router';
import Actions from 'Core/Redux/Actions';
import { formatCurrency } from 'Common/helper';
import { urlHelper } from 'Common/utils';
import moment from 'moment';

const DetailRoomsPage = (props) => {
  const dispatch = useDispatch();
  const history = useHistory();
  const { detailRooms } = useSelector((state) => state.rooms);

  const id_manager_room = _lodash.get(
    props,
    'match.params.id_manager_room',
    null
  );
  const isRooms = !!_lodash.get(detailRooms, 'info_room', null);

  const [arrDataSelectdService, setArrDataSelectedService] = useState(
    isRooms ? [id_manager_room] : []
  );

  useEffect(() => {
    if (!id_manager_room) return;

    dispatch(
      Actions.Rooms.getDetailRooms({
        data: {
          id_manager_room,
        },
      })
    );
  }, [dispatch, props, id_manager_room]);
  useEffect(() => {
    const dataServiceFollowRoom = detailRooms?.services?.map(
      (item) => item.id_manager_service
    );
    return setArrDataSelectedService(dataServiceFollowRoom);
  }, [detailRooms]);

  const { Search } = Input;
  const onSearch = (value) => console.log(value);

  const handleSubmitAddBooking = async (_fields) => {
    const checkin = new Date(_lodash.get(_fields, 'date_booking[0]', Date()));
    const checkout = new Date(_lodash.get(_fields, 'date_booking[1]', Date()));

    const dataRes = await Actions.Rooms.actOrderBookingRoom({
      amount_room: _fields.amount_room,
      price: _lodash.get(detailRooms, 'info_room.price', ''),
      checkin: checkin.getTime(),
      checkout: checkout.getTime(),
      id_manager_room,
      services: arrDataSelectdService || [id_manager_room],
      amount_service: _fields.amount_service,
    });
    const { data } = dataRes;

    if (data.status === 'SUCCESS') {
      history.push(urlHelper.getUrlToListOrder().route.to);
    }
  };
  function disabledDate(current) {
    // Can not select days before today and today
    return (
      (current && current <= moment().endOf('day')) ||
      current.diff(Date(), 'days') > 30
    );
  }
  return (
    <div className="wrap-page-detail">
      <div className="wrap-page-detail__background">
        <Carousel>
          {_lodash
            .get(
              detailRooms,
              'info_room.images',
              _lodash.get(detailRooms, 'info_service.images', [])
            )
            .map((itemImage) => {
              return (
                <img
                  src={`http://localhost:5003/${itemImage}` || detailBackground}
                  alt="bg-detail"
                />
              );
            })}
        </Carousel>
      </div>
      <div className="wrap-page-detail__content">
        <div>
          <ul>
            <ul>
              <li>
                <a href="#">Tổng quan</a>
              </li>
              <li>
                <a href="#">Địa điểm</a>
              </li>
              <li>
                <a href="#">Phản hồi</a>
              </li>
            </ul>
          </ul>
        </div>
      </div>
      <div className="wrap-page-detail__body">
        <div className="wrap-page-detail__body__left">
          <div className="card__info">
            <div className="card__info--header">
              <h3 className="card__info--title">
                {_lodash.get(detailRooms, 'info_room.title', '')}
              </h3>
            </div>
            <div className="card__info--rate">
              <Rate
                className="rate_icon"
                style={{ fontSize: '11px', color: 'rgb(1, 127, 255)' }}
                disabled
                value={_lodash.get(detailRooms, 'info_room.rate', 0)}
              />
            </div>
            <div className="card__info--content">
              {_lodash.get(detailRooms, 'info_room.description', '')}
            </div>
          </div>
          <div className="card__service">
            <div className="card__service--title">Dịch vụ</div>
            <div className="card__service--content">
              <div className="wrap_list_card_service">
                <div className="card_service_intro">
                  <svg
                    stroke="currentColor"
                    fill="currentColor"
                    strokeWidth="0"
                    viewBox="0 0 640 512"
                    height="1em"
                    width="1em"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path d="M634.91 154.88C457.74-8.99 182.19-8.93 5.09 154.88c-6.66 6.16-6.79 16.59-.35 22.98l34.24 33.97c6.14 6.1 16.02 6.23 22.4.38 145.92-133.68 371.3-133.71 517.25 0 6.38 5.85 16.26 5.71 22.4-.38l34.24-33.97c6.43-6.39 6.3-16.82-.36-22.98zM320 352c-35.35 0-64 28.65-64 64s28.65 64 64 64 64-28.65 64-64-28.65-64-64-64zm202.67-83.59c-115.26-101.93-290.21-101.82-405.34 0-6.9 6.1-7.12 16.69-.57 23.15l34.44 33.99c6 5.92 15.66 6.32 22.05.8 83.95-72.57 209.74-72.41 293.49 0 6.39 5.52 16.05 5.13 22.05-.8l34.44-33.99c6.56-6.46 6.33-17.06-.56-23.15z" />
                  </svg>
                </div>
                <div className="card_service_intro">
                  <svg
                    stroke="currentColor"
                    fill="currentColor"
                    strokeWidth="0"
                    viewBox="0 0 480 512"
                    height="1em"
                    width="1em"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path d="M438.66 212.33l-11.24-28.1-19.93-49.83C390.38 91.63 349.57 64 303.5 64h-127c-46.06 0-86.88 27.63-103.99 70.4l-19.93 49.83-11.24 28.1C17.22 221.5 0 244.66 0 272v48c0 16.12 6.16 30.67 16 41.93V416c0 17.67 14.33 32 32 32h32c17.67 0 32-14.33 32-32v-32h256v32c0 17.67 14.33 32 32 32h32c17.67 0 32-14.33 32-32v-54.07c9.84-11.25 16-25.8 16-41.93v-48c0-27.34-17.22-50.5-41.34-59.67zm-306.73-54.16c7.29-18.22 24.94-30.17 44.57-30.17h127c19.63 0 37.28 11.95 44.57 30.17L368 208H112l19.93-49.83zM80 319.8c-19.2 0-32-12.76-32-31.9S60.8 256 80 256s48 28.71 48 47.85-28.8 15.95-48 15.95zm320 0c-19.2 0-48 3.19-48-15.95S380.8 256 400 256s32 12.76 32 31.9-12.8 31.9-32 31.9z" />
                  </svg>
                </div>
                <div className="card_service_intro">
                  <svg
                    stroke="currentColor"
                    fill="currentColor"
                    strokeWidth="0"
                    viewBox="0 0 640 512"
                    height="1em"
                    width="1em"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path d="M189.61 310.58c3.54 3.26 15.27 9.42 34.39 9.42s30.86-6.16 34.39-9.42c16.02-14.77 34.5-22.58 53.46-22.58h16.3c18.96 0 37.45 7.81 53.46 22.58 3.54 3.26 15.27 9.42 34.39 9.42s30.86-6.16 34.39-9.42c14.86-13.71 31.88-21.12 49.39-22.16l-112.84-80.6 18-12.86c3.64-2.58 8.28-3.52 12.62-2.61l100.35 21.53c25.91 5.53 51.44-10.97 57-36.88 5.55-25.92-10.95-51.44-36.88-57L437.68 98.47c-30.73-6.58-63.02.12-88.56 18.38l-80.02 57.17c-10.38 7.39-19.36 16.44-26.72 26.94L173.75 299c5.47 3.23 10.82 6.93 15.86 11.58zM624 352h-16c-26.04 0-45.8-8.42-56.09-17.9-8.9-8.21-19.66-14.1-31.77-14.1h-16.3c-12.11 0-22.87 5.89-31.77 14.1C461.8 343.58 442.04 352 416 352s-45.8-8.42-56.09-17.9c-8.9-8.21-19.66-14.1-31.77-14.1h-16.3c-12.11 0-22.87 5.89-31.77 14.1C269.8 343.58 250.04 352 224 352s-45.8-8.42-56.09-17.9c-8.9-8.21-19.66-14.1-31.77-14.1h-16.3c-12.11 0-22.87 5.89-31.77 14.1C77.8 343.58 58.04 352 32 352H16c-8.84 0-16 7.16-16 16v32c0 8.84 7.16 16 16 16h16c38.62 0 72.72-12.19 96-31.84 23.28 19.66 57.38 31.84 96 31.84s72.72-12.19 96-31.84c23.28 19.66 57.38 31.84 96 31.84s72.72-12.19 96-31.84c23.28 19.66 57.38 31.84 96 31.84h16c8.84 0 16-7.16 16-16v-32c0-8.84-7.16-16-16-16zm-512-96c44.18 0 80-35.82 80-80s-35.82-80-80-80-80 35.82-80 80 35.82 80 80 80z" />
                  </svg>
                </div>
                <div className="card_service_intro">
                  <svg
                    stroke="currentColor"
                    fill="currentColor"
                    strokeWidth="0"
                    viewBox="0 0 384 512"
                    height="1em"
                    width="1em"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path d="M378.94 321.41L284.7 224h49.22c15.3 0 23.66-16.6 13.86-27.53L234.45 69.96c3.43-6.61 5.55-14 5.55-21.96 0-26.51-21.49-48-48-48s-48 21.49-48 48c0 7.96 2.12 15.35 5.55 21.96L36.22 196.47C26.42 207.4 34.78 224 50.08 224H99.3L5.06 321.41C-6.69 333.56 3.34 352 21.7 352H160v32H48c-8.84 0-16 7.16-16 16v96c0 8.84 7.16 16 16 16h288c8.84 0 16-7.16 16-16v-96c0-8.84-7.16-16-16-16H224v-32h138.3c18.36 0 28.39-18.44 16.64-30.59zM192 31.98c8.85 0 16.02 7.17 16.02 16.02 0 8.84-7.17 16.02-16.02 16.02S175.98 56.84 175.98 48c0-8.85 7.17-16.02 16.02-16.02zM304 432v32H80v-32h224z" />
                  </svg>
                </div>
              </div>
              <div className="read_more">Xem tất cả</div>
            </div>
          </div>
          <div className="card__location">
            <div className="card__location--title">Địa điểm</div>
            <div className="card__location--content">
              <p>
                <iframe
                  src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3919.4555217959482!2d106.66548831461421!3d10.776381292321547!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31752f9023a3a85d%3A0xdee5c99a7b02feab!2sHuflit!5e0!3m2!1svi!2s!4v1618556149474!5m2!1svi!2s"
                  width="100%"
                  height="450"
                  loading="lazy"
                  title="location rooms"
                />
              </p>
            </div>
          </div>
          <div className="card_review">
            <div className="card_review--header">
              <div className="card_review--header--left">
                <span className="card_review--header--title">35 Phản hồi</span>
                <Rate
                  className="rate_icon"
                  style={{ fontSize: '11px', color: 'rgb(1, 127, 255)' }}
                  disabled
                  defaultValue={5}
                />
              </div>
              <div className="card_review--header--right">
                <Search
                  placeholder="phản hồi"
                  onSearch={onSearch}
                  // style={{ width: 200, height: 40 }}
                />
                <Button type="primary">viết 1 phản hồi</Button>
              </div>
            </div>
            <div className="wrap__list__review">
              <div className="card_review--table">
                <div className="card_review--table--title">
                  Khách du lịch đánh giá
                </div>
                <div className="wrap_list_checkbox_review">
                  <div className="list_review_check">
                    <div className="item_wrap">
                      <Checkbox />
                      <span className="title">Xuất sắc</span>
                    </div>
                    <div className="item_wrap">
                      <Checkbox />
                      <span className="title">Rất tốt</span>
                    </div>
                    <div className="item_wrap">
                      <Checkbox />
                      <span className="title">Khá</span>
                    </div>
                    <div className="item_wrap">
                      <Checkbox />
                      <span className="title">Tệ</span>
                    </div>
                  </div>

                  <div className="list_review_check">
                    <div className="item_wrap">
                      <Rate
                        className="rate_icon"
                        style={{ fontSize: '11px', color: 'rgb(1, 127, 255)' }}
                        disabled
                        value={5}
                        tooltips={2}
                      />
                      <span className="title">(73)</span>
                    </div>
                    <div className="item_wrap">
                      <Rate
                        className="rate_icon"
                        style={{ fontSize: '11px', color: 'rgb(1, 127, 255)' }}
                        disabled
                        value={3}
                        tooltips={3}
                      />
                      <span className="title">(92)</span>
                    </div>
                    <div className="item_wrap">
                      <Rate
                        className="rate_icon"
                        style={{ fontSize: '11px', color: 'rgb(1, 127, 255)' }}
                        disabled
                        value={1}
                        tooltips={1}
                      />
                      <span className="title">(34)</span>
                    </div>
                    <div className="item_wrap">
                      <Rate
                        className="rate_icon"
                        style={{ fontSize: '11px', color: 'rgb(1, 127, 255)' }}
                        disabled
                        value={2}
                        tooltips={5}
                      />
                      <span className="title">(11)</span>
                    </div>
                  </div>
                </div>
              </div>
              <div className="card_review--table">
                <div className="card_review--table--title">
                  Khách du lịch đánh giá
                </div>
                <div className="wrap_list_checkbox_review">
                  <div className="list_review_check">
                    <div className="item_wrap">
                      <Checkbox />
                      <span className="title">Xuất xắc</span>
                    </div>
                    <div className="item_wrap">
                      <Checkbox />
                      <span className="title">Rất tốt</span>
                    </div>
                    <div className="item_wrap">
                      <Checkbox />
                      <span className="title">Khá</span>
                    </div>
                    <div className="item_wrap">
                      <Checkbox />
                      <span className="title">Tệ</span>
                    </div>
                  </div>
                </div>
              </div>
              <div className="card_review--table">
                <div className="card_review--table--title">
                  Khách du lịch đánh giá
                </div>
                <div className="wrap_list_checkbox_review">
                  <div className="list_review_check">
                    <div className="item_wrap">
                      <Checkbox />
                      <span className="title">Xuất xắc</span>
                    </div>
                    <div className="item_wrap">
                      <Checkbox />
                      <span className="title">Rất tốt</span>
                    </div>
                    <div className="item_wrap">
                      <Checkbox />
                      <span className="title">Khá</span>
                    </div>
                    <div className="item_wrap">
                      <Checkbox />
                      <span className="title">Tệ</span>
                    </div>
                  </div>
                </div>
              </div>
              <div className="card_review--table">
                <div className="card_review--table--title">
                  Khách du lịch đánh giá
                </div>
                <div className="wrap_list_checkbox_review">
                  <div className="list_review_check">
                    <div className="item_wrap">
                      <Checkbox />
                      <span className="title">Xuất xắc</span>
                    </div>
                    <div className="item_wrap">
                      <Checkbox />
                      <span className="title">Rất tốt</span>
                    </div>
                    <div className="item_wrap">
                      <Checkbox />
                      <span className="title">Khá</span>
                    </div>
                    <div className="item_wrap">
                      <Checkbox />
                      <span className="title">Tệ</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="wrap-page-detail__body__right">
          <div className="wrap_modal_booking">
            <div className="header">
              <div className="wrap_price">
                <span className="money">
                  {formatCurrency(
                    _lodash.get(
                      detailRooms,
                      'info_room.price',
                      _lodash.get(detailRooms, 'info_service.price', 0)
                    )
                  )}
                </span>
                <span> / {isRooms ? 'đêm' : 'suất'} </span>
              </div>
              {/* <div className="contact_owner">
                Contact {_lodash.get(detailRooms, 'type_room.model', '')}
              </div> */}
            </div>
            <div className="content">
              {_lodash.get(
                detailRooms,
                'info_room',
                _lodash.get(detailRooms, 'info_service', {})
              ) && (
                <Form
                  className="form_input_book"
                  onFinish={handleSubmitAddBooking}
                >
                  {isRooms && (
                    <>
                      <Form.Item
                        label="Thời gian đặt"
                        name="date_booking"
                        className="item_form"
                      >
                        <DatePicker.RangePicker
                          disabledDate={disabledDate}
                          ranges={{
                            'This Month': [
                              moment().startOf('month'),
                              moment().endOf('month'),
                            ],
                          }}
                        />
                      </Form.Item>
                      <Form.Item
                        label="Số Lượng"
                        name="amount_room"
                        className="item_form"
                        initialValue={1}
                      >
                        <Input type="number" />
                      </Form.Item>
                      <Form.Item
                        name="services"
                        label="Dịch vụ đi kèm"
                        className="item_form"
                        // initialValue={renderDefailtValueForSelect()}
                      >
                        <Select
                          mode="multiple"
                          defaultValue={arrDataSelectdService}
                          onChange={(res) => {
                            return setArrDataSelectedService(res);
                          }}
                        >
                          {_lodash
                            .get(detailRooms, 'services', [])
                            .map((item) => (
                              <Select.Option
                                value={item.id_manager_service}
                                key={item.id_manager_service}
                              >
                                {item.info_service.title}
                              </Select.Option>
                            ))}
                        </Select>
                      </Form.Item>
                    </>
                  )}
                  <Form.Item
                    label="Số Lượng dịch vụ"
                    name="amount_service"
                    className="item_form"
                    initialValue={1}
                  >
                    <Input type="number" />
                  </Form.Item>
                  <Button
                    type="primary"
                    htmlType="submit"
                    className="btnBookRoom"
                  >
                    Đặt {isRooms ? 'phòng' : 'dịch vụ'}
                  </Button>
                </Form>
              )}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
export default DetailRoomsPage;
