import React from 'react';
import { notification } from 'antd';
import './notification.scss';

const NotificationHOC = (WrapComponent) => {
  const showNotification = (_obj) => {
    console.log(_obj);
    const __TYPE = _obj?.type || 'SUCCESS';
    const _TITLE = _obj?.title || 'Team A6 Resort';
    const _DESCRIPTION = _obj?.description || 'Notification Team A6 Resort';
    let StyleNofication = {
      color: 'white',
      fontWeight: 'bold',
    };
    switch (__TYPE) {
      case 'SUCCESS':
        StyleNofication = {
          ...StyleNofication,
          backgroundColor: '#00e600',
        };
        break;
      case 'ERROR':
        StyleNofication = {
          ...StyleNofication,
          backgroundColor: '#ff3333',
        };
        break;
      default:
        StyleNofication = {
          ...StyleNofication,
        };
        break;
    }
    return notification.info({
      style: { ...StyleNofication },
      className: `NotificationHOC ${__TYPE}`,
      message: _TITLE,
      description: _DESCRIPTION,
      placement: 'topRight',
    });
  };
  return (props) => (
    <WrapComponent {...(props || {})} showNotification={showNotification} />
  );
};
export default NotificationHOC;
