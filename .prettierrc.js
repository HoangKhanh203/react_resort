module.exports = {
  singleQuote: true,
  endOfLine: 'crlf',
  tabWidth: 2,
};
